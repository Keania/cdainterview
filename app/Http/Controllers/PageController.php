<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    //
    
    /**
     * Index view to list all pages
     * in our cms
     *
     * @return void
     */
    public function index()
    {
        $pages  = Page::all();

        return view('cms.pages', ['pages'=>$pages]);
    }

    
    /**
     * Returns a page to view a single post item
     *
     * @param  \App\Page $page
     * @return void
     */
    public function view(Page $page)
    {
        return view('cms.view_page', ['page'=>$page, 'posts'=> $page->posts]);
    }

    
    /**
     * Creates a page
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function create(Request $request)
    {
        try {

            $page  = Page::create($request->except(['_token','banner_img']));

            if ($request->hasFile('banner_img') && $request->file('banner_img')->isValid()) {
                
                $filename = $request->file('banner_img')->store('', 'banners');

                $page->update(['banner_img'=>$filename]);
            }
        

        } catch (\Exception $e) {

            return redirect()->back()->with('failure', $e->getMessage());
        }

        return redirect()->back()->with('success', 'Page Created');
    }

    
    /**
     * Updates 
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Page $page
     * @return void
     */
    public function update(Request $request, Page $page)
    {
        try {

            $page->update($request->except(['_token', 'banner_img']));

            if ($request->hasFile('banner_img') && $request->file('banner_img')->isValid()) {

                $filename = $request->file('banner_img')->store('', 'banners');

                if ($page->banner_img) {

                    Storage::disk('banners')->delete($page->banner_img);
                }

                $page->update(['banner_img'=>$filename]);
            }
        } catch (\Exception $e) {

            return redirect()->back()->with('failure', $e->getMessage());
        }

        return redirect()->back()->with('success', 'Page Updated');
    }

    
    /**
     * Tooggle site no-index
     *
     * @param  \App\Page $page
     * @return void
     */
    public function toggleNoIndex(Page $page)
    {
        try {

            $page->update(['no_index'=> !$page->no_index]);
            
        }catch (\Exception $e) {

            return redirect()->back()->with('failure', $e->getMessage());
        }

        return redirect()->back()->with('success', 'No Index Updated');
    }

    
    
    /**
     * Updates page meta data
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Page $page
     * @return void
     */
    public function updateMeta(Request $request, Page $page)
    {
        try {

            $siteMetas = $request->site_metas;

            $defaultMeta = (object) ['name'=>'author', 'contents'=>'Keania Eric'];

            $data = json_decode($siteMetas);

            array_push($data, $defaultMeta);


            $page->update(['site_metas'=>json_encode($data)]);

        }catch (\Exception $e) {

            return redirect()->back()->with('failure', $e->getMessage());
        }

        return redirect()->back()->with('success', 'Page Meta Updated');
    }

    
    /**
     * Updates the banner for a page
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Page $page
     * @return void
     */
    public function UpdateBanner(Request $request, Page $page)
    {
        
    }
}
