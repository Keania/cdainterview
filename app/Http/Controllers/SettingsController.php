<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    //
    
    /**
     *Our constructor method
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    /**
     * The view to display all settings
     *
     * @return void
     */
    public function index()
    {
        $settings = Setting::all();
        return  view('cms.settings', ['settings'=> $settings]);
    }
    
    /**
     * Update Site Settings
     *
     * @param  mixed $request
     * @return void
     */
    public function update(Request $request)
    {
        try {

            foreach($request->all() as $index=>$value) {

                $setting = Setting::where('slug', $index)->first();

                if ($setting) {

                    $setting->update(['value'=> $value]);
                }
            }
        }catch(\Exception $e) {

            return redirect()->back()->with('errors', $e->getMessage());
        }

        return redirect()->back()->with('success', 'Settings Updated');
    }
}
