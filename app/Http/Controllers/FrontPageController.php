<?php

namespace App\Http\Controllers;

use App\Setting;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FrontPageController extends Controller
{
    //
    
    /**
     * This methods returns the main page view
     *
     * @return void
     */
    public function main()
    {
        return view('main');
    }
    
    /**
     * This method returns the contact page view
     *
     * @return void
     */
    public function contact()
    {
        $email = Setting::where('slug', 'site_email')->first()->email;
        return view('contact', ['email'=>$email]);
    }

    
    /**
     * This method is called to send the email
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function makeContact(Request $request)
    {
        try {
            
            $sendToMail = Setting::where('slug', 'site_email')->first()->value;

            if (! $sendToMail) {

                throw new \Exception('Could not find a mail to send to');
            }

            //backend validation of request
            $this->validate(
                $request,
                [
                    'name'=>'required|string',
                    'email'=>'required|string',
                    'help'=>'required|string'
                ] 
            );

            $data = $request->only(['name','help','email']);

           
            Mail::to($sendToMail)->send(new ContactMail($data));

        } catch (\Exception $e) {
            
            return redirect()->back()->with('failure', $e->getMessage());
        }

        $msg = 'Thank you, your email has been sent.';

        $msg .= ' We will get back to you shortly.';
        
        return redirect()->back()->with('success', $msg);
    }
}
