<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * The method we call to create our post
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
           
            $this->validate(
                $request, [
                    'page_id'=>'required',
                    'contents'=> 'required'
                ]
            );

            Post::create($request->only(['page_id', 'contents']));

        }catch (\Exception $e) {

            return redirect()->back()->with('failure', $e->getMessage());
        }

        return redirect()->back()->with('success', 'Post Added');
    }

    
    /**
     * Update the post data
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Post $post
     * @return void
     */
    public function update(Request $request, Post $post)
    {
        try {

            $post->update($request->except(['_token']));

        } catch(\Exception $e) {
            return redirect()->back()->with('errors', $e->getMessage());
        }

        return redirect()->back()->with('success', 'Post Updated');
    }
    
    /**
     * The method for deleteing a post
     *
     * @param  \App\Post $post
     * @return void
     */
    public function delete(Post $post)
    {
        try {

            $post->delete();

        } catch(\Exception $e) {
            return redirect()->back()->with('errors', $e->getMessage());
        }

        return redirect()->back()->with('success', 'Post Deleted');
    }
}
