<?php

namespace App\Console\Commands;

use App\Page;
use App\Setting;
use Illuminate\Console\Command;

class InitSettings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setup:application';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates application setting vars';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        $settings = [
            'site_email'=> ' Site Email Address',
            'facebook_pixel' => ' Facebook Ad Pixel',
            'google_analytics'=> 'Google Analytic Tag'
        ];

        foreach($settings as $index=>$value) {

            Setting::create(['name'=>$value, 'slug'=>$index]);
        }


        // setup default pages

        $pages = [
            'Main' => 'Home Page ',
            'Contact'=> 'Contact Page'
        ];

        $defaultMeta = [(object) ['name'=>'author', 'contents'=>'Keania Eric']];

        $order = 0;

        foreach ($pages as $index=>$value) {

            $order++;

            Page::create(
                [
                    'name'=>$index, 
                    'title'=>$value, 
                    'order'=>$order, 
                    'site_metas'=> json_encode($defaultMeta)
                ]
            );
        }


        // setup user

        factory(\App\User::class)->create([
            'email'=>'cdainterview@gmail.com',
            'password'=>bcrypt('cdainterview@2020')
        ]);




    }
}
