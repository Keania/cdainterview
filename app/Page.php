<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    //
    protected $guarded = [];

    
    /**
     * Get all posts relating to this page
     *
     * @return void
     */
    public function posts()
    {
        return $this->hasMany(\App\Post::class);
    }
}
