<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontPageController@main')->name('main');

Route::get('/contact', 'FrontPageController@contact')->name('contact');

Route::post(
    '/make/contact', 'FrontPageController@makeContact'
)->name('contact.post');

Route::get('/panel', 'Auth\LoginController@showLoginForm')->name('login-panel');
Auth::routes();

Route::get('/register', function (){
    abort(404);
});
Route::group(
    ['prefix'=>'auth', 'middleware'=>'auth'], function () {

        Route::get('/settings', 'SettingsController@index')->name('site.settings');

        Route::post('/settings/update', 'SettingsController@Update')->name('update.settings');
        
        Route::get('/pages', 'PageController@index')->name('pages.index');

        Route::get('/page/{page}', 'PageController@view')->name('page.view');

        Route::post('/create', 'PageController@create')->name('page.create');

        Route::post(
            '/page/update/{page}', 'PageController@update'
        )->name('page.update');

        Route::post(
            '/page/toggle/no_index/{page}',
            'PageController@toggleNoIndex'
        )->name('page.toggle-no-index');

        Route::post(
            '/page/update/metas/{page}', 'PageController@updateMeta'
        )->name('page.update-meta');

        Route::post(
            '/page/update/banner/{page}', 'PageController@updateBanner'
        )->name('page.update-banner');

        Route::post(
            '/page/update/banner-text/{page}', 'PageController@updateBannerText'
        )->name('page.update-banner-text');


        Route::group(
            ['prefix'=>'posts'], function () {

                Route::post(
                    '/create/{page}', 'PostController@create'
                )->name('add.post');

                Route::post(
                    '/update/{post}', 'PostController@update'
                )->name('update.post');

                Route::post('/delete/{post}', 'PostController@delete')->name('delete.post');
            }
        );
    }
);

Route::get('/home', 'HomeController@index')->name('home');
