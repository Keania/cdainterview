@extends('layouts.front')
@php
    $page = \App\Page::where('name', strtolower('main'))->first();
    $posts = $page->posts;

    $facebook_pixel = \App\Setting::where('slug', 'facebook_pixel')->first();
    $google_analytics = \App\Setting::where('slug', 'google_analytics')->first();
@endphp

@section('page-metas')

    @if($page)

        <title>{{$page->name}}</title>

        @forelse(json_decode($page->site_metas) as $meta)
            <meta name="{{$meta->name}}" content="{{$meta->contents}}">
        @empty
        @endforelse

        @if($page->no_index)
            <meta name="robots" content="noindex">
        @endif

        <!-- Over here we are going to include the settings -->
        @if($facebook_pixel)
            {!! $facebook_pixel->value !!}
        @endif

        @if($google_analytics)
            {!! $google_analytics->value !!}
        @endif
        

    @endif

@endsection

@section('page-contents')

    <!-- The main contents of the application starts here -->

    
    <main>

        @if ($page)

            @if (isset($page->banner_img))
                <section id="landing-image">
                    <img src="{{asset('storage/banners/'.$page->banner_img)}}" class="img-fluid">
                    @if ($page->banner_img_text)
                        <h1 class="page-image-text">{{$page->banner_img_text}}</h1>
                    @endif
                </section>
            @endif

            <section id="posts" style="padding:47px;">
                @forelse($posts as $post)
                    <article>
                        {!! $post->contents !!}
                    </article>
                @empty
                    <p>No post available for this page</p>
                @endforelse
            </section>

        @else 
            <h1 class="text-center">Contents for Main page is not set</h1>
        @endif
    </main>
    <!-- The main contents of the application ends here -->
@endsection