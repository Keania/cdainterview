@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">{{$page->name}} Page</div>

        <div class="card-body">

            <div>
                <form method="POST" action="{{route('page.update', ['page'=>$page->id])}}" enctype="multipart/form-data">
                    @csrf 

                    @if(isset($page->banner_img)) 
                        <img src="{{asset('storage/banners/'.$page->banner_img)}}" class="img-fluid">
                    @endif

                    <div class="form-group">
                        <label class="">
                            Change Banner Image
                        </label>

                        <input type="file" name="banner_img" class="form-control col-md-6 ">
                    </div>

                    <div class="form-group">

                        <label class="">
                            Banner Text
                        </label>

                        <input type="text" name="banner_img_text" class="form-control col-md-6" value="{{$page->banner_img_text}}">
                    </div>

                    <div>
                        <button type="submit" class="btn btn-primary">Update Page Banners</button>
                    </div>
                </form>
            <div>

            <div class="mt-3">

                <div class="float-right" style="clear:both;">
                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#metamodal">Update Page Meta</button>
                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#postmodal">Add Post</button>
                </div>

                <table class="table table-responsive">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>Post</th>
                        </tr>
                    </thead>

                    <tbody>
                        @forelse($posts as $post)
                            <tr>
                                <td>{{$loop->index + 1}}</td>
                                <td>{{substr($post->contents, 1, 20)}}</td>
                                <td>
                                    <form class="form-inline" method="POST" action="{{route('delete.post', ['post'=>$post->id])}}">
                                        @csrf

                                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                    </form>
                                </td>

                                <td>
                                    <a href="#" data-target="#editpost{{$post->id}}" data-toggle="modal"><i class="fa fa-2x fa-edit"></i></a>

                                    <div class="modal" tabindex="-1" role="dialog" id="editpost{{$post->id}}">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Edit Post</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form id="editpost-form{{$post->id}}" method="POST" action="{{route('update.post', ['post'=>$post->id])}}">
                                                    @csrf
                                                    <div>
                                                    
                                                        <post :contents="{{ json_encode($post->contents) }}"></post>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary" onclick="document.getElementById('editpost-form{{$post->id}}').submit()">Save</button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <p>No Post Attached to this page</p>
                        @endforelse
                    </tbody>
                </table>

            </div>

           

        </div>
    </div>


    <div class="modal" tabindex="-1" role="dialog" id="metamodal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Meta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="sit_meta_form" method="POST" action="{{route('page.update-meta', ['page'=>$page->id])}}">
                    @csrf
                    
                    <site-meta :metas="{{$page->site_metas}}"></site-meta>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" onclick="document.getElementById('sit_meta_form').submit()">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>


    <div class="modal" tabindex="-1" role="dialog" id="postmodal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add A Post</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="addpost" method="POST" action="{{route('add.post', ['page'=>$page->id])}}">
                    @csrf
                    <div>
                        <input type="hidden" name="page_id" value="{{$page->id}}">
                        <post></post>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" onclick="document.getElementById('addpost').submit()">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>

   
@endsection
