@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">My Pages</div>

        <div class="card-body">
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Page Title</th>
                        <th>Order</th>
                        <th>Delete</th>
                        <th>No Index</th>
                    </tr>
                </thead>

                <tbody>
                    @forelse($pages as $page)
                        <tr>
                            <td>{{$loop->index + 1}}</td>
                            <td>
                                <a href="{{route('page.view', ['page'=>$page->id])}}">{{$page->name}}</a>
                            </td>
                            <td>{{$page->order}}</td>
                    
                            <td>No Action Yet</td>

                            <td>
                               
                                <form method="POST" action="{{route('page.toggle-no-index', ['page'=>$page->id])}}">
                                    @csrf
                                    @if($page->no_index) 
                                        <button class="btn btn-danger btn-sm" type="submit">Turn Off</button>
                                    @endif

                                    @if(! $page->no_index) 
                                        <button class="btn btn-primary btn-sm" type="submit">Turn On</button>
                                    @endif
                                </form>
                                
                            </td>
                        </tr>
                    @empty
                        <p>No Page is available Yet</p>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
