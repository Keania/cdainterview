@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">Site Settings</div>

        <div class="card-body">
            <form action="{{route('update.settings')}}" method="POST">
                @csrf

                @forelse($settings as $setting)
                    <div class="form-group">
                        <label class="form-control-label">{{$setting->name}}</label>

                        @if($setting->slug == 'google_analytics' || $setting->slug == 'facebook_pixel')
                            <textarea name="{{$setting->slug}}" class="form-control">{{$setting->value}}</textarea>
                        @else
                            <input type="text" name="{{$setting->slug}}" class="form-control" value="{{$setting->value}}">
                        @endif
                       
                    </div>
                @empty
                    <p>No settings available</p>
                @endforelse
                
                <div class="form-group">
                    <button type="submit" class="btn btn-sm btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection
