<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        @yield('page-metas')

        
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <!-- This is my custom style for the site -->
        <link rel="stylesheet" href="{{asset('css/custom.css')}}">

        <script src="https://kit.fontawesome.com/3bd2d46499.js" crossorigin="anonymous"></script>

    <body>
            <!-- The webpage header starts here -->
            <header class="navbar navbar-expand-md fixed-top navbar-default">

               
                <!-- Brand -->
                    <div class="navbar-brand">
                        <a class="" href="#">
                            <img width="167" height="100" src="{{asset('images/bemo-logo2.png')}}">
                        </a>
                    </div>
                   

                    <!-- Toggler/collapsibe Button -->
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                        <!-- <span class="navbar-toggler-icon"></span> -->
                        <div class="toggler-stroke"></div>
                        <div class="toggler-stroke"></div>
                        <div class="toggler-stroke"></div>
                    </button>

                    <!-- Navbar links -->
                    <div class="collapse navbar-collapse " id="collapsibleNavbar">

                        <ul class="navbar-nav" id="topnav">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('main')}}">Main</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">How To Prepare</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">CDA Interview Questions</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('contact')}}">Contact Us</a>
                            </li>
                        </ul>

                    </div>

               

            </header>

            <!-- The webpage header ends here -->

            @yield('page-contents')


            <footer>

                <div id="footer-top"> 
                    &copy;2013 - 2016 BeMo Academic Consulting Inc. All rights reserved. <a href="#">Disclaimer & Privacy Policy</a>
                    <a href="#">Contact Us</a>
                </div>

                <div id="footer-bottom"> 
                    <a href="#" class="social-links" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="social-links" target="_blank"><i class="fa fa-twitter"></i></a>
                </div>
                
            </footer>

            <a href="#" id="scrollTop"></a>
    </body>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</html>
