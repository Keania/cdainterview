@extends('layouts.front')
@php
    $page = \App\Page::where('name', strtolower('contact'))->first();
    $posts = $page->posts;

    $facebook_pixel = \App\Setting::where('slug', 'facebook_pixel')->first();
    $google_analytics = \App\Setting::where('slug', 'google_analytics')->first();

    $email = \App\Setting::where('slug', 'site_email')->first();
@endphp

@section('page-metas')

    @if($page)
        <title>{{$page->name}}</title>

        @forelse(json_decode($page->site_metas) as $meta)
            <meta name="{{$meta->name}}" content="{{$meta->contents}}">
        @empty
        @endforelse

        @if($page->no_index)
            <meta name="robots" content="noindex">
        @endif

        <!-- Over here we are going to include the settings -->
        @if($facebook_pixel)
            {!! $facebook_pixel->value !!}
        @endif

        @if($google_analytics)
            {!! $google_analytics->value !!}
        @endif
        

    @endif

@endsection

@section('page-contents')

    <!-- The main contents of the application starts here -->
    <main>

            @if (isset($page->banner_img))
                <section id="landing-image">
                    <img src="{{asset('storage/banners/'.$page->banner_img)}}" class="img-fluid">
                    @if ($page->banner_img_text)
                        <h1 class="page-image-text">{{$page->banner_img_text}}</h1>
                    @endif
                </section>
            @endif

        <section id="contact-form">

                @if(session()->has('success'))
                    <div class="alert alert-success">{{session()->get('success')}}</div>
                @endif

                @if(session()->has('failure'))
                    <div class="alert alert-danger">{{session()->get('failure')}}</div>
                @endif
                <h5>BeMo Academic Consulting Inc.</h5>
                <div>
                    <div><span class="underline">Toll Free</span> 1-855-900-BeMo (2366)</div>
                    <div><span class="underline">Email</span>{{$email ? $email->value : ''}}</div>
                </div>
            


            <form id="contact-form" method="POST" action="{{route('contact.post')}}">

                @csrf
                <div>
                    <label>Name *</label>
                    <input type="text" name="name" id="sender_name" class="form-input">
                </div>

                <div>
                    <label>Email Address *</label>
                    <input type="text" name="email" id="sender_email" class="form-input">
                </div>

                <div>
                    <label>How can we help you ? *</label>
                    <textarea class="form-input" id="sender_help" name="help"></textarea>
                </div>


                <div id="form-buttons"> 
                    <button class="" onclick="event.preventDefault(); resetForm();">Reset</button>
                    <button class="">Submit</button>
                </div>

            </form>

            <p> <span class="underline"><b>Note: </b></span>
            If you are having difficulties with our contact us form above, send us an email to  {{$email ? $email->value : ''}} (copy & paste the email address)
            </p>
        </section>

    </main>
    <!-- The main contents of the application ends here -->

    <script>
        function resetForm() {

            get('sender_name');
            get('sender_email');
            get('sender_help')
        }

        function get(name) {
            document.getElementById(name).value = '';
        }
    </script>
@endsection