<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Page;
use Faker\Generator as Faker;

$factory->define(Page::class, function (Faker $faker) {
    return [
        //
        'name'=>$faker->firstName,
        'title'=>$faker->jobTitle,
        'order'=> $faker->numberBetween($min = 1, $max=20),
        
    ];
});
