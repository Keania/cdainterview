<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [

        'page_id'=> function ($item) {

            return factory(\App\Page::class)->create()->id;
        },

        'contents'=> $faker->randomHtml(2, 3)
    ];
});
