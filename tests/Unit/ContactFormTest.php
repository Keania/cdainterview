<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Mail\ContactMail;
use Illuminate\Support\Facades\Mail;



class ContactFormTest extends TestCase
{
    
    /**
     * Test setUp method @overide
     *
     * @return void
     */
    public function setUp():void
    {
        parent::setUp();

        $this->faker = \Faker\Factory::create();
    }
    /**
     * Test that there is a successful
     * response when i visit the contact
     * route
     *
     * @return void
     */
    public function testContactPageOk()
    {
        $route = route('contact');
        $response = $this->get($route);
        $response->assertStatus(200);
        $response->assertSeeText('Academic Consulting');
    }
    
    /**
     * A simple test for when post contact form 
     * submits with missing requirements
     *
     * @return void
     */
    public function testMakePostFailsValidation()
    {
        Mail::fake();

        $data = [
            'name'=> $this->faker->firstName,
            'email'=> null,
            'help'=> null
        ];

        $route = route('contact.post');
        $response = $this->post($route, $data);
        $response->assertSessionHas(['errors'=>'The given data was invalid.']);

        $response->assertStatus(302);

        Mail::assertNothingSent();
    }

     
    /**
     * A test that mail gets sent with the right data
     *
     * @return void
     */
    public function testMakePostSucceeds()
    {
        Mail::fake();

        $data = [
            'name'=> $this->faker->firstName,
            'email'=> $this->faker->safeEmail,
            'help'=> $this->faker->sentence($nbWords = 6, $variableNbWords = true) 
        ];
        
        $route = route('contact.post');

        $response = $this->post($route, $data);

        $response->assertSessionHas(
            [
            'success'=>
            'Thank you, your email has been sent. We will get back to you shortly.'
            ]
        );
       

        $response->assertStatus(302);

        Mail::assertSent(ContactMail::class);
    }
}
