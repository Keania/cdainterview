<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostTest extends TestCase
{
    /**
     * Test we can add a post
     *
     * @return void
     */
    public function testAddPost()
    {

        $page = factory(\App\Page::class)->create();

        $user = factory(\App\User::class)->create();

        $post = factory(\App\Post::class)->make(['page_id'=>$page->id])->toArray();

        $url = route('add.post');

        $response = $this->actingAs($user)->post($url, $post);

        $response->assertSessionHas(['success'=>'Post Added']);
    }


    /**
     * Test we can update a post
     *
     * @return void
     */
    public function testUpdatePost()
    {

        $user = factory(\App\User::class)->create();

        $faker = \Faker\Factory::create();

        $post = factory(\App\Post::class)->create();

        $url = route('update.post', ['post'=>$post->id]);

        $data = ['contents'=> $faker->randomHtml(2, 3)];

        $response = $this->actingAs($user)->post($url, $data);

        $response->assertSessionHas(['success'=>'Post Updated']);
    }


    /**
     * Test we can delete a post
     *
     * @return void
     */
    public function testDeletePost()
    {

        $user = factory(\App\User::class)->create();

        $post = factory(\App\Post::class)->create();

        $url = route('delete.post', ['post'=>$post->id]);

        $response = $this->actingAs($user)->post($url);

        $response->assertSessionHas(['success'=>'Post Deleted']);
    }


    


    
}
