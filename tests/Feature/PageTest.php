<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PageTest extends TestCase
{
    /**
     * Test the page view can be shown
     *
     * @return void
     */
    public function testPageIndex()
    {
        $user = factory(\App\User::class)->create();

        $url  = route('pages.index');

        $response = $this->actingAs($user)->get($url);

        $response->assertStatus(200);
    }
    
    /**
     * Test we can be a single page
     *
     * @return void
     */
    public function testPageSingle()
    {
        $user = factory(\App\User::class)->create();

        $page = factory(\App\Page::class)->create();

        $url  = route('page.view', ['page'=>$page->id]);

        $response = $this->actingAs($user)->get($url);

        $response->assertStatus(200);
    }

    
    /**
     * Test we can create a  new page
     *
     * @return void
     */
    public function testAddPage()
    {
        $user = factory(\App\User::class)->create();

        $url  = route('page.create');

        $page = factory(\App\Page::class)->make()->toArray();

        $response = $this->actingAs($user)->post($url, $page);

        $response->assertSessionHas(['success'=>'Page Created']);
    }
    
    /**
     * A test that we can update  a page
     *
     * @return void
     */
    public function testUpdatePage()
    {
        $user = factory(\App\User::class)->create();

        $page = factory(\App\Page::class)->create();

        $url  = route('page.update', ['page'=>$page->id]);

        $data = ['title'=>'No title'];

        $response = $this->actingAs($user)->post($url, $data);

        $response->assertSessionHas(['success'=>'Page Updated']);
    }
    
    /**
     * A test that we can update site metas
     *
     * @return void
     */
    public function testUpdateSiteMetas()
    {
        $user = factory(\App\User::class)->create();

        $page = factory(\App\Page::class)->create();

        $url  = route('page.update', ['page'=>$page->id]);

        $meta1 = (object) [
            'name'=>'author',
            'content'=>'Keania Johnson'
        ];

        $meta2 = (object) [
            'name'=>'description',
            'content'=>'Be my guest jenny'
        ];

        $metas = array($meta1, $meta2);

        $data = ['site_metas'=> json_encode($metas)];

        $response = $this->actingAs($user)->post($url, $data);

        $response->assertSessionHas(['success'=>'Page Updated']);
    }

    
    /**
     * Test no index toggling works
     *
     * @return void
     */
    public function testToggleNoIndex()
    {
        $user = factory(\App\User::class)->create();

        $page = factory(\App\Page::class)->create();

        $url  = route('page.toggle-no-index', ['page'=>$page->id]);

        $response = $this->actingAs($user)->post($url);

        $response->assertSessionHas(['success'=>'No Index Updated']);
    }

    
    /**
     * Test we can update a banner text
     *
     * @return void
     */
    public function testUpdateBannerTest()
    {
        $user = factory(\App\User::class)->create();

        $page = factory(\App\Page::class)->create();

        $url  = route('page.update', ['page'=>$page->id]);

        $data = ['banner_img_text'=> 'A new Banner Text'];

        $response = $this->actingAs($user)->post($url);

        $response->assertSessionHas(['success'=>'Page Updated']);
    }

    
    /**
     * Test we can update a banner image
     *
     * @return void
     */
    public function testUpdateBanner()
    {
        Storage::fake('public');
        
        $user = factory(\App\User::class)->create();

        $page = factory(\App\Page::class)->create();

        $url  = route('page.update', ['page'=>$page->id]);

        $data = [
            'banner_img'=> $file = UploadedFile::fake()->image('avatar.jpg')
        ];

        $response = $this->actingAs($user)->post($url, $data);

        $page->refresh();

        //$this->assertEquals('banners/'.$file->hashName(), $page->banner_img);
        
        dump(Storage::url($file->hashName()));
        Storage::disk('public')->assertExists('/storage/'.$file->hashName());

        $response->assertSessionHas(['success'=>'Page Updated']);
    }



    

    
    

    

   
}
